package cz.wz.markaos.workbooks;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import cz.wz.markaos.workbooks.util.database.Contract;

public class SearchActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        if(Intent.ACTION_VIEW.equals(intent.getAction())) {
            String id = intent.getData().getLastPathSegment();
            SQLiteDatabase db = new Contract.Helper((WorkbooksApplication) getApplication()).getReadableDatabase();

            String[] columns = {Contract.Workbooks.COLUMN_NAME_URL};
            String selection = Contract.Workbooks._ID + "=?";
            String[] selectionArgs = {id};
            Cursor c = db.query(Contract.Workbooks.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
            if(c.getCount() < 1) {
                throw new RuntimeException("Got 0 results :(");
            }

            c.moveToFirst();
            String url = c.getString(c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_URL));
            Intent i = new Intent();
            i.setAction(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
        finish();
    }
}
