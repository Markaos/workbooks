package cz.wz.markaos.workbooks.adapters;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.wz.markaos.workbooks.GroupSelectionActivity;
import cz.wz.markaos.workbooks.R;
import cz.wz.markaos.workbooks.WorkbooksApplication;
import cz.wz.markaos.workbooks.util.database.Contract;


public class GroupsAdapter {
    private class GSHolder extends ArrayList<GroupsAdapter.GHolder> {
        private GSHolder(String n, String i) {
            super();
            name = n;
            id = i;
        }

        public String name;
        public String id;
    }

    private class GHolder {
        private GHolder(String n, String i) {
            name = n;
            id = i;
        }
        public String name;
        public String id;
    }

    private int groupsCount;

    public GroupsAdapter(RadioGroup.OnCheckedChangeListener act, WorkbooksApplication ctx, LinearLayout into) {
        this(act, ctx, into, null);
    }

    public GroupsAdapter(RadioGroup.OnCheckedChangeListener act, WorkbooksApplication ctx, LinearLayout into, Map<String, String> groups) {
        SQLiteDatabase db = new Contract.Helper(ctx).getReadableDatabase();
        String []columns = {
                Contract.Groups.COLUMN_NAME_GSNAME,
                Contract.Groups.COLUMN_NAME_NAME,
                Contract.Groups.COLUMN_NAME_GID,
                Contract.Groups.COLUMN_NAME_GSID
        };
        Cursor c = db.query(Contract.Groups.TABLE_NAME, columns, null, null, null, null, Contract.Groups.COLUMN_NAME_GSID + " ASC");
        int []cols = {
                c.getColumnIndex(Contract.Groups.COLUMN_NAME_GSNAME),
                c.getColumnIndex(Contract.Groups.COLUMN_NAME_NAME),
                c.getColumnIndex(Contract.Groups.COLUMN_NAME_GID),
                c.getColumnIndex(Contract.Groups.COLUMN_NAME_GSID)
        };

        Map<String, GroupsAdapter.GSHolder> gmap = new HashMap<>();
        while(c.moveToNext()) {
            String gsname = c.getString(cols[0]);
            String name = c.getString(cols[1]);
            String gid = c.getString(cols[2]);
            String gsid = c.getString(cols[3]);

            if(gmap.containsKey(gsid)) {
                gmap.get(gsid).add(new GroupsAdapter.GHolder(name, gid));
            } else {
                GroupsAdapter.GSHolder gsh = new GroupsAdapter.GSHolder(gsname, gsid);
                gsh.add(new GroupsAdapter.GHolder(name, gid));
                gmap.put(gsid, gsh);
            }
        }

        c.close();
        groupsCount = gmap.size();

        for(GroupsAdapter.GSHolder gsh : gmap.values()) {
            View wrapper = LayoutInflater.from(ctx).inflate(R.layout.item_group_group, into, false);
            ((TextView) wrapper.findViewById(R.id.groups_title)).setText(gsh.name);
            RadioGroup r = (RadioGroup) wrapper.findViewById(R.id.groups_radio);
            r.setTag(gsh.id);
            for(GroupsAdapter.GHolder gh : gsh) {
                RadioButton b = (RadioButton) LayoutInflater.from(ctx).inflate(R.layout.item_group_item, r, false);
                b.setText(gh.name);
                b.setTag(gh.id);
                r.addView(b);
                if(groups != null && gh.id.equals(groups.get(gsh.id))) {
                    Log.d("GroupsAdapter", "Setting group");
                    r.check(b.getId());
                }
            }
            r.setOnCheckedChangeListener(act);
            into.addView(wrapper);
        }
    }

    public int getGroupsCount() {
        return groupsCount;
    }
}
