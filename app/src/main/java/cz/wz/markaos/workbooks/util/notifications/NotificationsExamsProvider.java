package cz.wz.markaos.workbooks.util.notifications;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.wz.markaos.workbooks.ListActivity;
import cz.wz.markaos.workbooks.R;
import cz.wz.markaos.workbooks.WorkbooksApplication;
import cz.wz.markaos.workbooks.util.database.Contract;

public class NotificationsExamsProvider {

    public static List<ExamInfo> getExams(Context ctx) {
        List<ExamInfo> list = new ArrayList<>();

        SQLiteDatabase db = new Contract.Helper(ctx).getWritableDatabase();
        SimpleDateFormat f = new SimpleDateFormat("d.M.y", Locale.US);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 1);
        String[] columns = {
                Contract.Exams.COLUMN_NAME_SUBJECT,
                Contract.Exams.COLUMN_NAME_RANGE,
                Contract.Exams.COLUMN_NAME_DATE,
                Contract.Exams.COLUMN_NAME_ID,
                Contract.Exams.COLUMN_NAME_GROUP
        };
        String selection = "(" + Contract.Exams.COLUMN_NAME_DATE + "=? OR " + Contract.Exams.COLUMN_NAME_DATE + "=?)" +
                " AND " + Contract.Exams.COLUMN_NAME_SEEN + "=0";
        String[] selectionArgs = {
                f.format(new Date()),
                f.format(cal.getTime())
        };
        Cursor c = db.query(Contract.Exams.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
        int[] cols = {
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_SUBJECT),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_RANGE),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_DATE),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_ID),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_GROUP)
        };

        String groups = ctx.getSharedPreferences("preload", 0).getString("groups", "NONE");

        while(c.moveToNext()) {
            String group = c.getString(cols[4]);
            if(!group.equals("none") && !groups.contains(group)) {
                continue;
            }
            int icon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? R.drawable.ic_event_note_white_24dp : R.drawable.ic_event_note_black_24dp;
            Date d;
            try {
                d = f.parse(c.getString(cols[2]));
            } catch (ParseException e) {
                d = new Date();
            }
            d.setHours(7);
            d.setMinutes(45);
            list.add(new ExamInfo(c.getInt(cols[3]), c.getString(cols[0]), c.getString(cols[1]), c.getString(cols[2]), d.getTime()));
        }
        c.close();
        ContentValues cv = new ContentValues();
        cv.put(Contract.Exams.COLUMN_NAME_SEEN, true);
        db.update(Contract.Exams.TABLE_NAME, cv, selection, selectionArgs);
        db.close();

        return list;
    }
}
