package cz.wz.markaos.workbooks.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import cz.wz.markaos.workbooks.WorkbooksApplication;
import cz.wz.markaos.workbooks.util.PreloadManager;

public class SyncMaster extends IntentService {

    public SyncMaster() {
        super("SyncMaster");
        Log.d("Sync", "Service created");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("Sync", "Processing request");
        ConnectivityManager cm =
                (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(!isConnected) return;

        boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        boolean eatAllTheData = getSharedPreferences("prefs", 0).getBoolean("eat_all_the_data", true);

        if(!isWiFi && !eatAllTheData) return;

        ((WorkbooksApplication) getApplication()).refreshFromBackground();
    }
}
