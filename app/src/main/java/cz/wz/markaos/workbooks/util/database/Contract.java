package cz.wz.markaos.workbooks.util.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import cz.wz.markaos.workbooks.WorkbooksApplication;

public class Contract {

    private Contract() {}

    public static class Workbooks implements BaseColumns {
        public static final String TABLE_NAME = "workbooks";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_NAME_FULL = "fullname";
        public static final String COLUMN_NAME_URL = "url";
        public static final String COLUMN_NAME_ARCHIVE = "archive";
        public static final String COLUMN_NAME_ICON = "icon";
        public static final String COLUMN_NAME_DEPRECATED = "dep";
    }

    public static class Exams implements BaseColumns {
        public static final String TABLE_NAME = "exams";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_SEEN = "seen";
        public static final String COLUMN_NAME_SUBJECT = "subject";
        public static final String COLUMN_NAME_RANGE = "range";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_DATE_I = "date_int";
        public static final String COLUMN_NAME_GROUP = "grp";
        public static final String COLUMN_NAME_LINK = "link";
        public static final String COLUMN_NAME_LINK_TYPE = "link_type";
    }

    public static class Groups implements BaseColumns {
        public static final String TABLE_NAME = "groups";
        public static final String COLUMN_NAME_GID = "gid";
        public static final String COLUMN_NAME_GSID = "gsid";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_GSNAME = "gsname";
    }

    public static class Subjects implements BaseColumns {
        public static final String TABLE_NAME = "subjects";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_CODE = "code";
        public static final String COLUMN_NAME_GID = "gid";
    }

    public static class Helper extends SQLiteOpenHelper {
        public static final int DATABASE_VERSION = 9;
        public static final String DATABASE_NAME = "db.db";

        private Context ctx;

        public Helper(Context ctx) {
            super(ctx.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
            this.ctx = ctx;
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + Workbooks.TABLE_NAME + " (" +
                Workbooks._ID + " INTEGER PRIMARY KEY," +
                Workbooks.COLUMN_NAME_DEPRECATED + " INTEGER, " +
                Workbooks.COLUMN_NAME_NAME + " TEXT, " +
                Workbooks.COLUMN_NAME_NAME_FULL + " TEXT, " +
                Workbooks.COLUMN_NAME_URL + " TEXT, " +
                Workbooks.COLUMN_NAME_ARCHIVE + " TEXT, " +
                Workbooks.COLUMN_NAME_ICON + " TEXT)");

            db.execSQL("CREATE TABLE " + Exams.TABLE_NAME + " (" +
                Exams._ID + " INTEGER PRIMARY KEY, " +
                Exams.COLUMN_NAME_ID + " INTEGER UNIQUE, " +
                Exams.COLUMN_NAME_SEEN + " INTEGER, " +
                Exams.COLUMN_NAME_DATE_I + " INTEGER, " +
                Exams.COLUMN_NAME_SUBJECT + " TEXT, " +
                Exams.COLUMN_NAME_RANGE + " TEXT, " +
                Exams.COLUMN_NAME_GROUP + " TEXT, " +
                Exams.COLUMN_NAME_LINK_TYPE + " TEXT, " +
                Exams.COLUMN_NAME_LINK + " TEXT, " +
                Exams.COLUMN_NAME_DATE + " TEXT)");

            db.execSQL("CREATE TABLE " + Groups.TABLE_NAME + " (" +
                Groups._ID + " INTEGER PRIMARY KEY, " +
                Groups.COLUMN_NAME_GID + " TEXT UNIQUE, " +
                Groups.COLUMN_NAME_GSID + " TEXT, " +
                Groups.COLUMN_NAME_NAME + " TEXT, " +
                Groups.COLUMN_NAME_GSNAME + " TEXT)");

            db.execSQL("CREATE TABLE " + Subjects.TABLE_NAME + " (" +
                Subjects._ID + " INTEGER PRIMARY KEY, " +
                Subjects.COLUMN_NAME_NAME + " TEXT UNIQUE, " +
                Subjects.COLUMN_NAME_CODE + " TEXT, " +
                Subjects.COLUMN_NAME_GID + " TEXT)");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if(oldVersion < 2) {
                db.execSQL("ALTER TABLE " + Exams.TABLE_NAME + " ADD COLUMN " + Exams.COLUMN_NAME_ID + " INTEGER");
                db.execSQL("UPDATE " + Exams.TABLE_NAME + " SET " + Exams.COLUMN_NAME_ID + "=0 WHERE 1");
                db.execSQL("ALTER TABLE " + Exams.TABLE_NAME + " ADD COLUMN " + Exams.COLUMN_NAME_SEEN + " INTEGER");
                db.execSQL("UPDATE " + Exams.TABLE_NAME + " SET " + Exams.COLUMN_NAME_ID + "=0 WHERE 1");
            }
            if(oldVersion < 3) {
                db.delete(Exams.TABLE_NAME, Exams.COLUMN_NAME_ID + "=0", null);
                db.execSQL("CREATE UNIQUE INDEX index_id ON " + Exams.TABLE_NAME + "(" + Exams.COLUMN_NAME_ID + ")");
            }
            if(oldVersion < 4) {
                SimpleDateFormat f = new SimpleDateFormat("d.M.y", Locale.US);

                String[] columns = {
                        Exams.COLUMN_NAME_ID,
                        Exams.COLUMN_NAME_DATE
                };
                String where = Exams.COLUMN_NAME_ID + "=?";
                String[] whereArgs = {
                        ""
                };

                ContentValues cv = new ContentValues();

                db.execSQL("ALTER TABLE " + Exams.TABLE_NAME + " ADD COLUMN " + Exams.COLUMN_NAME_DATE_I + " INTEGER");
                Cursor c = db.query(Exams.TABLE_NAME, columns, null, null, null, null, null);
                int[] cols = {
                        c.getColumnIndex(Exams.COLUMN_NAME_ID),
                        c.getColumnIndex(Exams.COLUMN_NAME_DATE)
                };

                while(c.moveToNext()) {
                    whereArgs[0] = Long.toString(c.getLong(cols[0]));
                    try {
                        long date = f.parse(c.getString(cols[1])).getTime();
                        cv.clear();
                        cv.put(Exams.COLUMN_NAME_DATE_I, date);
                        db.update(Exams.TABLE_NAME, cv, where, whereArgs);
                    } catch (ParseException e) {
                        db.delete(Exams.TABLE_NAME, where, whereArgs);
                    }
                }
                c.close();
            }
            if(oldVersion < 5) {
                db.execSQL("DELETE FROM " + Exams.TABLE_NAME + " WHERE " + Exams._ID + " NOT IN " +
                    "(SELECT MIN(" + Exams._ID + ") FROM " + Exams.TABLE_NAME + " GROUP BY " + Exams.COLUMN_NAME_ID + ")");
                db.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS index_id ON " + Exams.TABLE_NAME + "(" + Exams.COLUMN_NAME_ID + ")");
            }
            if(oldVersion < 6) {
                ContentValues cv = new ContentValues();
                cv.put(Exams.COLUMN_NAME_SEEN, false);
                db.update(Exams.TABLE_NAME, cv, "1", null);
            }
            if(oldVersion < 7) {
                ContentValues cv = new ContentValues();
                cv.put(Exams.COLUMN_NAME_SEEN, false);
                db.update(Exams.TABLE_NAME, cv, "1", null);
            }
            if(oldVersion < 8) {
                db.execSQL("ALTER TABLE " + Exams.TABLE_NAME + " ADD COLUMN " + Exams.COLUMN_NAME_GROUP + " TEXT");
                db.execSQL("CREATE TABLE " + Groups.TABLE_NAME + " (" +
                        Groups._ID + " INTEGER PRIMARY KEY, " +
                        Groups.COLUMN_NAME_GID + " TEXT UNIQUE, " +
                        Groups.COLUMN_NAME_GSID + " TEXT, " +
                        Groups.COLUMN_NAME_NAME + " TEXT, " +
                        Groups.COLUMN_NAME_GSNAME + " TEXT)");
                db.execSQL("CREATE TABLE " + Subjects.TABLE_NAME + " (" +
                        Subjects._ID + " INTEGER PRIMARY KEY, " +
                        Subjects.COLUMN_NAME_NAME + " TEXT UNIQUE, " +
                        Subjects.COLUMN_NAME_CODE + " TEXT, " +
                        Subjects.COLUMN_NAME_GID + " TEXT)");
            }
            if(oldVersion < 9) {
                db.execSQL("ALTER TABLE " + Exams.TABLE_NAME + " ADD COLUMN " + Exams.COLUMN_NAME_LINK_TYPE + " TEXT");
                db.execSQL("ALTER TABLE " + Exams.TABLE_NAME + " ADD COLUMN " + Exams.COLUMN_NAME_LINK + " TEXT");
                db.execSQL("UPDATE " + Exams.TABLE_NAME + " SET " + Exams.COLUMN_NAME_LINK + "=\"none\" WHERE 1");
            }
            if(ctx instanceof WorkbooksApplication) ((WorkbooksApplication) ctx).refresh();
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Backward compatibility isn't needed
        }
    }
}
