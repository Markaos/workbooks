package cz.wz.markaos.workbooks;

import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import cz.wz.markaos.workbooks.util.ApiManager;


public class LoginActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        final EditText codeEdit = findViewById(R.id.settings_code);

        findViewById(R.id.settings_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String code = codeEdit.getText().toString().toUpperCase();
                        if(ApiManager.login(getApplicationContext(), code)) {
                            getApplicationContext().getSharedPreferences("user", 0).edit().putString("code", code).apply();
                            ((WorkbooksApplication) getApplication()).refreshFromBackground();
                            finish();
                        } else {
                            Looper.prepare();
                            Toast.makeText(getApplicationContext(), R.string.settings_invalid_code, Toast.LENGTH_SHORT).show();
                        }
                    }
                }).start();
            }
        });
    }
}
