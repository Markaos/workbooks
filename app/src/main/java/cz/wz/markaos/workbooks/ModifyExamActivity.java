package cz.wz.markaos.workbooks;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.iid.InstanceID;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cz.wz.markaos.workbooks.util.ApiManager;
import cz.wz.markaos.workbooks.util.PreloadManager;
import cz.wz.markaos.workbooks.util.database.Contract;

public class ModifyExamActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_em_modify);

        EditText dat = (EditText) findViewById(R.id.em_date);
        EditText range = (EditText) findViewById(R.id.em_range);

        SQLiteDatabase db = new Contract.Helper((WorkbooksApplication) getApplication()).getReadableDatabase();

        String[] columns = {
                Contract.Exams.COLUMN_NAME_RANGE,
                Contract.Exams.COLUMN_NAME_DATE
        };
        String[] args = {Integer.toString(((WorkbooksApplication) getApplication()).retrieveExamId())};
        Cursor c = db.query(Contract.Exams.TABLE_NAME, columns, Contract.Exams.COLUMN_NAME_ID + "=?", args, null, null, null);
        int[] index = {
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_RANGE),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_DATE)
        };

        c.moveToFirst();

        dat.setText(c.getString(index[1]));
        range.setText(c.getString(index[0]));

        c.close();
        db.close();

        dat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ModifyExamActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "DatePickerDialog");
            }
        });
        dat.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b) view.performClick();
            }
        });

        findViewById(R.id.em_add_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String range = ((EditText) findViewById(R.id.em_range)).getText().toString();
                final String date = ((EditText) findViewById(R.id.em_date)).getText().toString();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String code = getApplicationContext().getSharedPreferences("user", 0).getString("code", "");
                        if(ApiManager.updateExam(getApplicationContext(), ((WorkbooksApplication) getApplication()).retrieveExamId(), range, date, false, code)) {
                            ((WorkbooksApplication) getApplication()).refreshFromBackground();
                            finish();
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ModifyExamActivity.this, ApiManager.getErrorMessageID(), Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                }).start();
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        ((EditText) findViewById(R.id.em_date)).setText(
                Integer.toString(dayOfMonth) + "." +
                        Integer.toString(monthOfYear + 1) + "." +
                        Integer.toString(year)
        );
    }
}
