package cz.wz.markaos.workbooks;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.wz.markaos.workbooks.adapters.GroupsAdapter;
import cz.wz.markaos.workbooks.util.database.Contract;

public class GroupSelectionActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    private Map<String, String> groups = new HashMap<>();

    @Override
    public void onCheckedChanged(RadioGroup group, int id) {
        groups.put((String) group.getTag(), (String) findViewById(id).getTag());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        LinearLayout layout = (LinearLayout) findViewById(R.id.groups_container);
        final GroupsAdapter adapter = new GroupsAdapter(this, (WorkbooksApplication) getApplication(), layout);

        ((Button) findViewById(R.id.button_done)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(groups.size() != adapter.getGroupsCount()) {
                    Toast.makeText(GroupSelectionActivity.this, R.string.group_fill_all, Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                String gstring = "";
                boolean first = true;
                for(Map.Entry<String, String> e : groups.entrySet()) {
                    if(!first) gstring += ",";
                    first = false;
                    gstring += e.getKey() + ":" + e.getValue();
                }
                getSharedPreferences("preload", 0).edit().putBoolean("groupsUpdate", true).apply();
                getSharedPreferences("preload", 0).edit().putString("groups", gstring).apply();
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
            }
        });
    }
}
