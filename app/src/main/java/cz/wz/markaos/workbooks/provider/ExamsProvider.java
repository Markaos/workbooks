package cz.wz.markaos.workbooks.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

import cz.wz.markaos.workbooks.WorkbooksApplication;
import cz.wz.markaos.workbooks.util.database.Contract;

public class ExamsProvider extends ContentProvider {

    @Override
    public String getType(@NonNull Uri uri) {
        return "vnd.android.cursor.dir/vnd.cz.markaos.wb.exams";
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String orderBy) {
        SQLiteDatabase db = new Contract.Helper((WorkbooksApplication) getContext().getApplicationContext()).getReadableDatabase();
        MatrixCursor ret = new MatrixCursor(projection);
        String groups = getContext().getSharedPreferences("preload", 0).getString("groups", "NONE");
        String[] columns = {
                Contract.Exams.COLUMN_NAME_SUBJECT,
                Contract.Exams.COLUMN_NAME_RANGE,
                Contract.Exams.COLUMN_NAME_DATE,
                Contract.Exams.COLUMN_NAME_GROUP,
                Contract.Exams.COLUMN_NAME_ID,
                Contract.Exams.COLUMN_NAME_LINK,
                Contract.Exams.COLUMN_NAME_LINK_TYPE,
                Contract.Exams.COLUMN_NAME_SEEN,
                Contract.Exams.COLUMN_NAME_DATE_I
        };
        Cursor c = db.query(Contract.Exams.TABLE_NAME, columns, null, null, null, null, Contract.Exams.COLUMN_NAME_DATE_I + " ASC");
        int[] index = {
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_SUBJECT),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_RANGE),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_DATE),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_GROUP),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_ID),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_LINK),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_LINK_TYPE),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_SEEN),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_DATE_I)
        };
        while(c.moveToNext()) {
            String group = c.getString(index[3]);
            if (!group.equals("none") && !groups.contains(group)) {
                continue;
            }

            MatrixCursor.RowBuilder row = ret.newRow();
            for (String col : projection) {
                switch (col) {
                    case Contract.Exams.COLUMN_NAME_SUBJECT:
                        row.add(c.getString(index[0]));
                        break;
                    case Contract.Exams.COLUMN_NAME_RANGE:
                        row.add(c.getString(index[1]));
                        break;
                    case Contract.Exams.COLUMN_NAME_DATE:
                        row.add(c.getString(index[2]));
                        break;
                    case Contract.Exams.COLUMN_NAME_GROUP:
                        row.add(c.getString(index[3]));
                        break;
                    case Contract.Exams.COLUMN_NAME_ID:
                        row.add(c.getInt(index[4]));
                        break;
                    case Contract.Exams.COLUMN_NAME_LINK:
                        row.add(c.getString(index[5]));
                        break;
                    case Contract.Exams.COLUMN_NAME_LINK_TYPE:
                        row.add(c.getString(index[6]));
                        break;
                    case Contract.Exams.COLUMN_NAME_SEEN:
                        row.add(c.getInt(index[7]));
                        break;
                    case Contract.Exams.COLUMN_NAME_DATE_I:
                        row.add(c.getInt(index[8]));
                        break;
                }
            }
        }
        c.close();

        return ret;
    }
}
