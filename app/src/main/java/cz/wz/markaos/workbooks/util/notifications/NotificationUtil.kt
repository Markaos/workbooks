package cz.wz.markaos.workbooks.util.notifications

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import android.support.v4.app.NotificationCompat
import cz.wz.markaos.workbooks.R

class NotificationUtil {
    companion object {
        const val CHANNEL_EXAMS: String = "channel_exams"

        @JvmStatic
        fun sendNotification(ctx: Context, id: Int, title: String, text: String, intent: PendingIntent, date: Long, channel: String, icon: Int = -1) {
            val iconr = if(icon == -1) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    R.drawable.ic_event_note_white_24dp
                } else {
                    R.drawable.ic_event_note_black_24dp
                }
            } else icon

            val manager = ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val notification = NotificationCompat.Builder(ctx, channel)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setContentIntent(intent)
                    .setWhen(date)
                    .setSmallIcon(iconr)
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .build()
            manager.notify(id, notification)
        }
    }
}