package cz.wz.markaos.workbooks.util;

import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

public class LinearLayoutWrapper {
    private LinearLayout list;

    public LinearLayoutWrapper(LinearLayout layout) {
        list = layout;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        for(int i = 0; i < adapter.getItemCount(); i++) {
            RecyclerView.ViewHolder viewHolder = adapter.onCreateViewHolder(list, 0);
            adapter.onBindViewHolder(viewHolder, i);
            list.addView(viewHolder.itemView);
        }
    }
}
