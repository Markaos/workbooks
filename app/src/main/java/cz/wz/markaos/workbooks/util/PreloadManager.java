package cz.wz.markaos.workbooks.util;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.gms.iid.InstanceID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import cz.wz.markaos.workbooks.WorkbooksApplication;
import cz.wz.markaos.workbooks.util.database.Contract;

public class PreloadManager {

    private static class IDHolder {
        public IDHolder(int i, boolean s) {
            id = i;
            seen = s;
        }

        private int id;
        private boolean seen;
    }

    // Taken from StackOverflow - http://stackoverflow.com/questions/10752919
    public static String getString(InputStream stream, String charsetName)
    {
        try {
            int n = 0;
            char[] buffer = new char[1024 * 4];
            InputStreamReader reader = new InputStreamReader(stream, charsetName);
            StringWriter writer = new StringWriter();
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            return writer.toString();
        } catch (Exception e) {
            Log.w("InputStream2String", e);
            return "";
        }
    }

    public static boolean loadAll(Context ctx) {
        String url = "https://zapisy.markaos.cz/workbooks.json";
        HttpsURLConnection connection = null;
        SQLiteDatabase db = (new Contract.Helper(ctx)).getWritableDatabase();
        try {
            List<String> icons = new ArrayList<>(15);
            connection = (HttpsURLConnection) (new URL(url)).openConnection();
            InputStream workbooks = connection.getInputStream();
            JSONArray wbs = new JSONArray(getString(workbooks, "utf8"));
            db.beginTransaction();
            db.delete(Contract.Workbooks.TABLE_NAME, "1", null);
            ContentValues values = new ContentValues();
            for(int i = 0; i < wbs.length(); i++) {
                values.clear();
                JSONObject wb = wbs.getJSONObject(i);
                values.put(Contract.Workbooks.COLUMN_NAME_NAME, wb.getString("name"));
                values.put(Contract.Workbooks.COLUMN_NAME_NAME_FULL, wb.optString("fullname", wb.getString("name")));
                values.put(Contract.Workbooks.COLUMN_NAME_URL, wb.getString("url"));
                values.put(Contract.Workbooks.COLUMN_NAME_ARCHIVE, wb.optString("archive", ""));
                values.put(Contract.Workbooks.COLUMN_NAME_ICON, wb.getString("icon").replace("img/", ""));
                values.put(Contract.Workbooks.COLUMN_NAME_DEPRECATED, wb.optBoolean("deprecated", false));
                db.insert(Contract.Workbooks.TABLE_NAME, null, values);
                icons.add(wb.getString("icon"));
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            if(!IconManager.loadIcons(ctx, icons)) {
                db.close();
                return false;
            }
        } catch (MalformedURLException e) {
            Log.e("Preload", "Wrong URL: " + url);
            db.close();
            return false;
        } catch (IOException e) {
            Log.e("Preload", "Cannot download " + url);
            db.close();
            return false;
        } catch (JSONException e) {
            Log.e("Preload", "Couldn't parse workbooks", e);
            db.close();
            return false;
        }

        url = "https://zapisy.markaos.cz/exams.json?v=3";
        try {
            connection = (HttpsURLConnection) (new URL(url)).openConnection();
            InputStream examStream = connection.getInputStream();
            JSONObject obj = new JSONObject(getString(examStream, "utf8"));
            JSONArray exams = obj.getJSONArray("exams");
            db.beginTransaction();
            SimpleDateFormat f = new SimpleDateFormat("d.M.y", Locale.US);
            ContentValues values = new ContentValues();
            List<IDHolder> ids = new ArrayList<>(8);

            String[] columns = {Contract.Exams.COLUMN_NAME_SEEN, Contract.Exams.COLUMN_NAME_ID};
            String[] seArgs = {""};
            String selection = Contract.Exams.COLUMN_NAME_ID + "=?";

            for(int i = 0; i < exams.length(); i++) {
                JSONObject wb = exams.getJSONObject(i);
                long date;
                try {
                    date = f.parse(wb.getString("date")).getTime();
                } catch (ParseException e) {
                    continue;
                }
                values.clear();
                values.put(Contract.Exams.COLUMN_NAME_SUBJECT, wb.getString("subject"));
                values.put(Contract.Exams.COLUMN_NAME_RANGE, wb.getString("range"));
                values.put(Contract.Exams.COLUMN_NAME_DATE, wb.getString("date"));
                values.put(Contract.Exams.COLUMN_NAME_GROUP, wb.optString("group", "none"));
                values.put(Contract.Exams.COLUMN_NAME_DATE_I, date);
                values.put(Contract.Exams.COLUMN_NAME_ID, wb.getInt("id"));
                values.put(Contract.Exams.COLUMN_NAME_SEEN, false);
                boolean seen = false;
                seArgs[0] = Integer.toString(wb.getInt("id"));
                Cursor c = db.query(Contract.Exams.TABLE_NAME, columns, selection, seArgs, null, null, null);
                if(c.getCount() > 0) {
                    c.moveToFirst();
                    seen = c.getInt(c.getColumnIndex(Contract.Exams.COLUMN_NAME_SEEN)) != 0;
                }
                c.close();

                JSONObject link = wb.optJSONObject("link");
                if(link != null) {
                    values.put(Contract.Exams.COLUMN_NAME_LINK_TYPE, link.getString("type"));
                    values.put(Contract.Exams.COLUMN_NAME_LINK, link.getString("data"));
                } else {
                    values.put(Contract.Exams.COLUMN_NAME_LINK, "none");
                }

                ids.add(new IDHolder(wb.getInt("id"), seen));
                db.insertWithOnConflict(Contract.Exams.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }

            StringBuilder builder = new StringBuilder();
            StringBuilder seen = new StringBuilder();
            builder.append("(");
            seen.append("(");
            boolean first = true;
            boolean firstSeen = true;
            for(IDHolder idh : ids) {
                if(!first) {
                    builder.append(", ");
                } else {
                    first = false;
                }
                builder.append(idh.id);

                if(idh.seen) {
                    if(!firstSeen) {
                        seen.append(", ");
                    } else {
                        firstSeen = false;
                    }
                    seen.append(idh.id);
                }
            }
            builder.append(")");
            seen.append(")");
            db.delete(Contract.Exams.TABLE_NAME, Contract.Exams.COLUMN_NAME_ID + " NOT IN " + builder.toString(), null);

            values.clear();
            values.put(Contract.Exams.COLUMN_NAME_SEEN, true);
            db.update(Contract.Exams.TABLE_NAME, values, Contract.Exams.COLUMN_NAME_ID + " IN " + seen.toString(), null);

            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (MalformedURLException e) {
            Log.e("Preload", "Wrong URL: " + url);
            db.close();
            return false;
        } catch (IOException e) {
            Log.e("Preload", "Cannot download " + url);
            db.close();
            return false;
        } catch (JSONException e) {
            Log.e("Preload", "Couldn't parse exams", e);
            db.close();
            return false;
        }

        url = "https://zapisy.markaos.cz/groups.json";
        try {
            db.beginTransaction();
            connection = (HttpsURLConnection) (new URL(url)).openConnection();
            InputStream result = connection.getInputStream();
            JSONArray root = new JSONArray(getString(result, "utf8"));
            ContentValues cv = new ContentValues();
            for(int i = 0; i < root.length(); i++) {
                JSONObject gs = root.getJSONObject(i);
                cv.put(Contract.Groups.COLUMN_NAME_GSID, gs.getString("gsid"));
                cv.put(Contract.Groups.COLUMN_NAME_GSNAME, gs.getString("gsname"));
                JSONArray groups = gs.getJSONArray("groups");
                for(int j = 0; j < groups.length(); j++) {
                    JSONObject group = groups.getJSONObject(j);
                    cv.put(Contract.Groups.COLUMN_NAME_GID, group.getString("gid"));
                    cv.put(Contract.Groups.COLUMN_NAME_NAME, group.getString("name"));
                    db.insertWithOnConflict(Contract.Groups.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (MalformedURLException e) {
            Log.e("Preload", "Wrong URL: " + url);
            db.close();
            return false;
        } catch (IOException e) {
            Log.e("Preload", "Cannot download " + url);
            db.close();
            return false;
        } catch (JSONException e) {
            Log.e("Preload", "Couldn't parse groups", e);
            db.close();
            return false;
        }

        url = "https://zapisy.markaos.cz/subjects.json";
        try {
            db.beginTransaction();
            connection = (HttpsURLConnection) (new URL(url)).openConnection();
            InputStream result = connection.getInputStream();
            JSONArray root = new JSONArray(getString(result, "utf8"));
            ContentValues cv = new ContentValues();
            for(int i = 0; i < root.length(); i++) {
                JSONObject sub = root.getJSONObject(i);
                String subname = sub.getString("name");
                cv.clear();
                cv.put(Contract.Subjects.COLUMN_NAME_NAME, subname);
                cv.put(Contract.Subjects.COLUMN_NAME_CODE, sub.getString("code"));
                String gsid = sub.optString("gsid", null);
                String cgid = sub.optString("cgid", null);
                if(gsid != null && cgid != null) {
                    if(cgid.contains("|")) {
                        for(String gid : cgid.split("\\|")) {
                            cv.put(Contract.Subjects.COLUMN_NAME_GID, gid);
                            cv.put(Contract.Subjects.COLUMN_NAME_NAME, subname + " (" + gid + ")");
                            db.insertWithOnConflict(Contract.Subjects.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                        }
                        continue;
                    } else {
                        cv.put(Contract.Subjects.COLUMN_NAME_GID, cgid);
                        cv.put(Contract.Subjects.COLUMN_NAME_NAME, subname + " (" + cgid + ")");
                    }
                } else if (gsid != null) {
                    String[] columns = {Contract.Groups.COLUMN_NAME_GID};
                    String selection = Contract.Groups.COLUMN_NAME_GSID + "=?";
                    String[] selectionArgs = {gsid};
                    Cursor c = db.query(Contract.Groups.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
                    while(c.moveToNext()) {
                        String gid = c.getString(c.getColumnIndex(Contract.Groups.COLUMN_NAME_GID));
                        cv.put(Contract.Subjects.COLUMN_NAME_GID, gid);
                        cv.put(Contract.Subjects.COLUMN_NAME_NAME, subname + " (" + gid + ")");
                        db.insertWithOnConflict(Contract.Subjects.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                    }
                    c.close();
                    continue;
                }
                db.insertWithOnConflict(Contract.Subjects.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (MalformedURLException e) {
            Log.e("Preload", "Wrong URL: " + url);
            db.close();
            return false;
        } catch (IOException e) {
            Log.e("Preload", "Cannot download " + url);
            db.close();
            return false;
        } catch (JSONException e) {
            Log.e("Preload", "Couldn't parse subjects", e);
            db.close();
            return false;
        }

        url = "https://zapisy.markaos.cz/motd.json";
        try {
            connection = (HttpsURLConnection) (new URL(url)).openConnection();
            InputStream result = connection.getInputStream();
            JSONObject root = new JSONObject(getString(result, "utf8"));
            SharedPreferences.Editor prefs = ctx.getSharedPreferences("preload", 0).edit();
            prefs.putString("motd_start", root.getString("start"));
            prefs.putString("motd_end", root.getString("end"));
            prefs.putString("motd_date", root.getString("date"));
            prefs.apply();
        } catch (MalformedURLException e) {
            Log.e("Preload", "Wrong URL: " + url);
            db.close();
            return false;
        } catch (IOException e) {
            Log.e("Preload", "Cannot download " + url);
            db.close();
            return false;
        } catch (JSONException e) {
            Log.e("Preload", "Couldn't parse groups", e);
            db.close();
            return false;
        }

        db.close();
        return true;
    }
}
