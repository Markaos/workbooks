package cz.wz.markaos.workbooks.workers

import android.app.PendingIntent
import android.content.Intent
import android.util.Log
import androidx.work.Worker
import cz.wz.markaos.workbooks.ListActivity
import cz.wz.markaos.workbooks.util.notifications.NotificationUtil
import cz.wz.markaos.workbooks.util.notifications.NotificationsExamsProvider

class NotifyWorker : Worker() {

    override fun doWork(): WorkerResult {
        Log.d("NotifyWorker", "NotifyWorker called")
        val exams = NotificationsExamsProvider.getExams(applicationContext)
        val act = PendingIntent.getActivity(applicationContext, 0, Intent(applicationContext, ListActivity::class.java), 0)
        for(exam in exams) {
            NotificationUtil.sendNotification (
                    ctx = applicationContext,
                    id = exam.id,
                    title = exam.subject,
                    text = exam.range + " (" + exam.date + ")",
                    intent = act,
                    date = exam.dateLong,
                    channel = NotificationUtil.CHANNEL_EXAMS
            )
        }
        return WorkerResult.SUCCESS
    }
}