package cz.wz.markaos.workbooks;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.iid.InstanceID;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cz.wz.markaos.workbooks.util.ApiManager;
import cz.wz.markaos.workbooks.util.PreloadManager;
import cz.wz.markaos.workbooks.util.database.Contract;


public class AddExamActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener {

    private String subject = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_em);

        String groups = getSharedPreferences("preload", 0).getString("groups", "NONE");
        SQLiteDatabase db = new Contract.Helper((WorkbooksApplication) getApplication()).getReadableDatabase();

        String[] columns = {
                Contract.Subjects.COLUMN_NAME_NAME,
                Contract.Subjects.COLUMN_NAME_GID
        };
        Cursor c = db.query(Contract.Subjects.TABLE_NAME, columns, null, null, null, null, null);
        int[] index = {
                c.getColumnIndex(Contract.Subjects.COLUMN_NAME_NAME),
                c.getColumnIndex(Contract.Subjects.COLUMN_NAME_GID)
        };

        final List<String> subjects = new ArrayList<>(c.getCount());

        while(c.moveToNext()) {
            String gid = c.getString(index[1]);
            if(gid == null || groups.contains(gid)) {
                subjects.add(c.getString(index[0]));
            }
        }

        c.close();
        db.close();

        BaseAdapter adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return subjects.size();
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                if(view == null) view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.item_subject, viewGroup, false);
                ((TextView) view).setText(subjects.get(i));
                view.setTag(subjects.get(i));
                return view;
            }
        };

        final Spinner spin = (Spinner) findViewById(R.id.em_subject);
        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(this);

        final AddExamActivity ctx = this;

        findViewById(R.id.em_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ctx,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "DatePickerDialog");
            }
        });
        findViewById(R.id.em_date).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b) view.performClick();
            }
        });

        findViewById(R.id.em_add_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String range = ((EditText) findViewById(R.id.em_range)).getText().toString();
                final String date = ((EditText) findViewById(R.id.em_date)).getText().toString();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String code = getApplicationContext().getSharedPreferences("user", 0).getString("code", "");
                        if(ApiManager.addExam(getApplicationContext(), subject, range, date, null, false, code)) {
                            ((WorkbooksApplication) getApplication()).refreshFromBackground();
                            finish();
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(AddExamActivity.this, ApiManager.getErrorMessageID(), Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                }).start();
            }
        });
    }

    @Override
    public void onNothingSelected(AdapterView group) {

    }

    @Override
    public void onItemSelected(AdapterView group, View v, int pos, long id) {
        subject = ((TextView) v).getText().toString();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        ((EditText) findViewById(R.id.em_date)).setText(
                Integer.toString(dayOfMonth) + "." +
                Integer.toString(monthOfYear + 1) + "." +
                Integer.toString(year)
        );
    }
}
