package cz.wz.markaos.workbooks.util;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class IconManager {

    private static final String iconPath = "/icons/";

    public static File getIconFile(Context ctx, String icon) {
        String root = ctx.getFilesDir().getAbsolutePath();
        return new File(root.concat(iconPath).concat(icon));
    }

    protected static boolean loadIcons(Context ctx, List<String> icons) {
        URL url;
        File f = new File(ctx.getFilesDir().getAbsolutePath().concat(iconPath));
        f.mkdir();
        for(String icon : icons) {
            try {
                url = new URL("https://zapisy.markaos.cz/" + icon);
            } catch (MalformedURLException e) {
                Log.e("IconMgr", "Wrong URL: https://zapisy.markaos.cz/" + icon);
                return false;
            }

            File out = null;
            try {
                out = new File(ctx.getFilesDir().getAbsolutePath().concat(iconPath).concat(icon.replace("img/", "")));
                if(out.exists()) continue;
                if(!out.exists() && !out.createNewFile()) {
                    throw new IOException("Cannot create new file " + out.getAbsolutePath());
                }
                URLConnection connection = url.openConnection();
                InputStream stream = connection.getInputStream();
                Log.d("IconMgr", "Downloading " + url.toString());
                OutputStream outputStream = new FileOutputStream(out);
                copyStream(stream, outputStream);
            } catch (IOException e) {
                Log.e("IconMgr", "Couldn't download https://zapisy.markaos.cz/" + icon);
                return false;
            }
        }
        return true;
    }

    private static void copyStream(InputStream input, OutputStream output)
            throws IOException
    {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }
}
