package cz.wz.markaos.workbooks;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.google.android.gms.iid.InstanceID;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import cz.wz.markaos.workbooks.util.ApiManager;
import cz.wz.markaos.workbooks.util.PreloadManager;

public class DeleteExamActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_em_delete);

        findViewById(R.id.em_cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteExamActivity.this.finish();
            }
        });

        findViewById(R.id.em_delete_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String code = getApplicationContext().getSharedPreferences("user", 0).getString("code", "");
                        if(ApiManager.deleteExam(DeleteExamActivity.this, ((WorkbooksApplication) getApplication()).retrieveExamId(), code)) {
                            ((WorkbooksApplication) getApplication()).refreshFromBackground();
                            finish();
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(DeleteExamActivity.this, ApiManager.getErrorMessageID(), Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                }).start();
            }
        });
    }
}
