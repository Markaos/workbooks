package cz.wz.markaos.workbooks.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import cz.wz.markaos.workbooks.ExamsFragment;
import cz.wz.markaos.workbooks.ListFragment;
import cz.wz.markaos.workbooks.R;
import cz.wz.markaos.workbooks.SettingsFragment;

public class OurPagerAdapter extends FragmentPagerAdapter {

    private Context ctx;

    public OurPagerAdapter(FragmentManager manager, Context context) {
        super(manager);
        ctx = context;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            default:
            case 0: return ctx.getResources().getText(R.string.tab_workbooks);
            case 1: return ctx.getResources().getText(R.string.tab_exams);
            case 2: return ctx.getResources().getText(R.string.tab_settings);
        }
    }

    @Override
    public Fragment getItem(int position) {
        Log.d("OurPagerAdapter", "getItem(" + position + ") called");
        switch(position) {
            default:
            case 0: return new ListFragment();
            case 1: return new ExamsFragment();
            case 2: return new SettingsFragment();
        }
    }
}
