package cz.wz.markaos.workbooks.adapters;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import cz.wz.markaos.workbooks.R;
import cz.wz.markaos.workbooks.WorkbooksApplication;
import cz.wz.markaos.workbooks.util.BackgroundGenerator;
import cz.wz.markaos.workbooks.util.database.Contract;

public class ExamsAdapter extends BaseAdapter {

    List<Exam> examList;

    private class Exam {
        public String subject, title, date;
        public int daysRemaining;

        public Exam(String subject, String title, String date, int daysRemaining) {
            this.subject = subject;
            this.title = title;
            this.date = date;
            this.daysRemaining = daysRemaining;
        }
    }

    public ExamsAdapter(WorkbooksApplication ctx) {
        examList = new ArrayList<>();
        SQLiteDatabase db = new Contract.Helper(ctx).getReadableDatabase();
        String[] columns = {
                Contract.Exams.COLUMN_NAME_SUBJECT,
                Contract.Exams.COLUMN_NAME_RANGE,
                Contract.Exams.COLUMN_NAME_DATE
        };
        Cursor c = db.query(Contract.Exams.TABLE_NAME, columns, null, null, null, null, Contract.Exams.COLUMN_NAME_DATE_I + " ASC");
        int[] index = {
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_SUBJECT),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_RANGE),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_DATE)
        };

        Calendar date = new GregorianCalendar();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        while(c.moveToNext()) {
            SimpleDateFormat f = new SimpleDateFormat("d.M.y", Locale.US);
            int days = -1;
            try {
                long diff = f.parse(c.getString(index[3])).getTime() - date.getTimeInMillis();
                double ddiff = (double) diff / (24 * 60 * 60 * 1000);
                if(ddiff > 0) days = (int) Math.ceil(ddiff);
                else days = (int) Math.floor(ddiff);
            } catch (ParseException e) {
                Log.e("Exams", "Cannot parse date " + c.getString(index[3]));
            }
            examList.add(new Exam(
                    c.getString(index[0]),
                    c.getString(index[1]),
                    c.getString(index[2]),
                    days
            ));
        }
        c.close();
        db.close();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int getCount() {
        return examList.size();
    }

    @Override
    public View getView(int position, View reusableView, ViewGroup root) {
        if(reusableView == null) {
            reusableView = LayoutInflater.from(root.getContext()).inflate(R.layout.item_exam, root, false);
        }

        ((TextView) reusableView.findViewById(R.id.item_exam_subject)).setText(examList.get(position).subject);
        ((TextView) reusableView.findViewById(R.id.item_exam_title)).setText(examList.get(position).title);
        ((TextView) reusableView.findViewById(R.id.item_exam_date)).setText(examList.get(position).date);
        if(examList.get(position).daysRemaining < 0) {
            if(Build.VERSION.SDK_INT >= 21) {
                reusableView.getBackground().setTint(Color.WHITE);
            } else if (Build.VERSION.SDK_INT >= 16) {
                reusableView.setBackground(ContextCompat.getDrawable(reusableView.getContext(), R.drawable.button_exam));
            }
        } else if (examList.get(position).daysRemaining <= 7) {
            if(Build.VERSION.SDK_INT >= 21) {
                reusableView.getBackground().setTint(BackgroundGenerator.getExamColor((float) examList.get(position).daysRemaining / (float) 7));
            } else if (Build.VERSION.SDK_INT >= 16) {
                reusableView.setBackground(BackgroundGenerator.generateExam(reusableView.getContext(),
                        R.drawable.button_exam, ((float) examList.get(position).daysRemaining / (float) 7)));
            }
        } else {
            Log.d("getView", "Resetting background...");
            if(Build.VERSION.SDK_INT >= 21) {
                reusableView.getBackground().setTint(Color.WHITE);
            } else if (Build.VERSION.SDK_INT >= 16) {
                reusableView.setBackground(ContextCompat.getDrawable(reusableView.getContext(), R.drawable.button_exam));
            }
        }

        return reusableView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }
}
