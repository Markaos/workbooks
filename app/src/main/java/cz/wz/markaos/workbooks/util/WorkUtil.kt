package cz.wz.markaos.workbooks.util

import androidx.work.*
import cz.wz.markaos.workbooks.workers.NotifyWorker
import cz.wz.markaos.workbooks.workers.SyncWorker
import java.util.concurrent.TimeUnit

class WorkUtil {
    companion object {
        const val TAG_NOTIFY = "worker_notify"
        const val TAG_SYNC = "worker_sync"

        @JvmStatic
        fun startAll() {
            startSynchronization()
            startNotifications()
        }

        @JvmStatic
        fun startNotifications() {
            startWork(TAG_NOTIFY, NotifyWorker::class.java, 6, TimeUnit.HOURS)
        }

        @JvmStatic
        fun startSynchronization() {
            val constraints = Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
            startWork(TAG_SYNC, SyncWorker::class.java, 12, TimeUnit.HOURS, constraints)
        }

        private fun startWork(tag: String, worker: Class<out Worker>, interval: Long, unit: TimeUnit, constraints: Constraints? = null) {
            val workManager = WorkManager.getInstance()
            workManager.cancelAllWorkByTag(tag)

            val builder = PeriodicWorkRequest.Builder(worker, interval, unit)
                    .addTag(tag)
            if(constraints != null) {
                builder.setConstraints(constraints)
            }

            val request = builder.build()

            workManager.enqueue(request)
        }
    }
}