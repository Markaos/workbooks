package cz.wz.markaos.workbooks.util;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;

public class BackgroundGenerator {

    public static Drawable generateExam(Context ctx, int resId, float fraction) {
        Drawable drawable = ContextCompat.getDrawable(ctx, resId);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, getExamColor(fraction));
        return drawable;
    }

    public static Drawable generateWorkbook(Context ctx, int resId, boolean deprecated) {
        Drawable drawable = ContextCompat.getDrawable(ctx, resId);
        drawable = DrawableCompat.wrap(drawable);
        int color = getWorkbookColor(deprecated);
        DrawableCompat.setTint(drawable, color);
        return drawable;
    }

    public static int getExamColor(float fraction) {
        //return (int) new ArgbEvaluator().evaluate(1 - fraction, Color.YELLOW, Color.RED);
        return Color.WHITE;
    }

    public static int getWorkbookColor(boolean deprecated) {
        return deprecated ? Color.rgb(220, 220, 220) : Color.rgb(255, 255, 255);
    }
}
