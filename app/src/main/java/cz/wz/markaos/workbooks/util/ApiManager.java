package cz.wz.markaos.workbooks.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import cz.wz.markaos.workbooks.R;

import static cz.wz.markaos.workbooks.util.PreloadManager.getString;

public class ApiManager {
    private static final String SERVER_URL = "https://zapisy.markaos.cz/api/v1/";
    private static int error = 0;

    private static boolean retrieveNonce(@NonNull Context ctx) {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) (new URL(SERVER_URL)).openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(true);
            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
            writer.write("action=getRequestId&iid=" + URLEncoder.encode(InstanceID.getInstance(ctx).getId(), "utf8"));
            writer.flush();
            writer.close();
            os.close();
            InputStream result = connection.getInputStream();
            String data = getString(result, "utf8");
            JSONObject root = new JSONObject(data);
            result.close();

            if(!root.getString("status").equals("OK")) {
                return false;
            }

            ctx.getSharedPreferences("ems", 0).edit().putString("nonce", root.getString("nonce")).apply();
            return true;
        } catch (ProtocolException e) {
            Log.e("ApiMgr", "Wrong protocol", e);
        } catch (IOException e) {
            Log.e("ApiMgr", "Unknown I/O exception", e);
        } catch (JSONException e) {
            Log.e("ApiMgr", "Bad JSON", e);
        }
        return false;
    }

    @Nullable
    private static boolean call(@NonNull Context ctx, @NonNull String action, @Nullable Map<String, String> parameters) {
        return call(ctx, action, parameters, true);
    }

    @Nullable
    private static boolean call(@NonNull Context ctx, @NonNull String action, @Nullable Map<String, String> parameters, boolean repeat) {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) (new URL(SERVER_URL)).openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(true);
            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
            writer.write("action=" + action + "&iid=" + URLEncoder.encode(InstanceID.getInstance(ctx).getId(), "utf8") +
            "&nonce=" + ctx.getSharedPreferences("ems", 0).getString("nonce", "NONE_ACQUIRED"));
            if(parameters != null) {
                for (Map.Entry<String, String> parameter : parameters.entrySet()) {
                    writer.write("&" + parameter.getKey() + "=" + URLEncoder.encode(parameter.getValue(), "utf8"));
                }
            }
            writer.flush();
            writer.close();
            os.close();
            InputStream result = connection.getInputStream();
            String data = getString(result, "utf8");
            Log.d("ApiMgr", data);
            JSONObject root = new JSONObject(data);
            result.close();

            if(root.getString("status").equals("error")) {
                if(root.getInt("code") == 1 && repeat) {
                    if(retrieveNonce(ctx)) {
                        return call(ctx, action, parameters, false);
                    }
                }
                error = root.getInt("code");
                return false;
            }

            if(root.has("nonce")) {
                ctx.getSharedPreferences("ems", 0).edit().putString("nonce", root.getString("nonce")).apply();
            }

            if(root.has("name")) {
                ctx.getSharedPreferences("user", 0).edit().putString("name", root.getString("name")).apply();
            }

            return true;
        } catch (ProtocolException e) {
            Log.e("ApiMgr", "Wrong protocol", e);
        } catch (IOException e) {
            Log.e("ApiMgr", "Unknown I/O exception", e);
        } catch (JSONException e) {
            Log.e("ApiMgr", "Bad JSON", e);
        }
        error = -1;
        return false;
    }

    public static boolean addExam(@NonNull Context ctx, @NonNull String subject, @NonNull String range, @NonNull String date, @Nullable String group, boolean uncertain, @NonNull String code) {
        if(group != null) {
            subject += " (" + group + ")";
        }
        Map<String, String> parameters = new HashMap<>();
        parameters.put("subject", subject);
        parameters.put("range", range);
        parameters.put("date", date);
        parameters.put("uc", uncertain ? "1" : "0");
        parameters.put("code", code);

        return call(ctx, "addExam", parameters);
    }

    public static boolean updateExam(@NonNull Context ctx, int id, @NonNull String range, @NonNull String date, boolean uncertain, @NonNull String code) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("eid", Integer.toString(id));
        parameters.put("range", range);
        parameters.put("date", date);
        parameters.put("uc", uncertain ? "1" : "0");
        parameters.put("code", code);

        return call(ctx, "updateExam", parameters);
    }

    public static boolean deleteExam(@NonNull Context ctx, int id, @NonNull String code) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("eid", Integer.toString(id));
        parameters.put("code", code);

        return call(ctx, "deleteExam", parameters);
    }

    public static boolean attachLink(@NonNull Context ctx, int id, @NonNull String link) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("eid", Integer.toString(id));
        parameters.put("link", link);

        return call(ctx, "attachLink", parameters);
    }

    public static boolean detachLink(@NonNull Context ctx, int eid) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("eid", Integer.toString(eid));

        return call(ctx, "detachLink", parameters);
    }

    public static boolean login(@NonNull Context ctx, String code) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("code", code);

        return call(ctx, "login", parameters);
    }

    public static int getError() {
        return error;
    }
    public static int getErrorMessageID() {
        switch(getError()) {
            case -1: return R.string.error_io;
            case 1: return R.string.error_wrong_nonce;
            case 3: return R.string.error_login_needed;
            case 4: return R.string.error_wrong_login;
            default: return R.string.error_deprecated;
        }
    }
}
