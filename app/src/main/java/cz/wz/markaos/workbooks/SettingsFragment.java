package cz.wz.markaos.workbooks;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import cz.wz.markaos.workbooks.adapters.GroupsAdapter;

public class SettingsFragment extends Fragment implements RadioGroup.OnCheckedChangeListener {

    private class SwitchListener implements CompoundButton.OnCheckedChangeListener {
        private String key;
        private Context ctx;

        public SwitchListener(Context ctx, String key) {
            this.key = key;
            this.ctx = ctx;
        }

        public void onCheckedChanged(CompoundButton b, boolean checked) {
            ctx.getSharedPreferences("prefs", 0).edit().putBoolean(key, checked).apply();
            ((WorkbooksApplication) getActivity().getApplication()).fakeRefresh();
            if (key.equals("bottom_navigation")) {
                getActivity().finish();
                getActivity().startActivity(getActivity().getIntent());
                getActivity().overridePendingTransition(0, 0);
            }
        }
    }

    private void initSwitch(View v, int id, String key, boolean def) {
        Switch widget = v.findViewById(id);
        widget.setChecked(getContext().getSharedPreferences("prefs", 0).getBoolean(key, def));
        widget.setOnCheckedChangeListener(new SwitchListener(getContext(), key));
    }

    private Map<String, String> groups = new HashMap<>();

    private void createLoginNeeded(LinearLayout user) { // Kinda ran out of ideas...
        LayoutInflater.from(getContext()).inflate(R.layout.item_user_login_prompt, user);
        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), LoginActivity.class);
                view.getContext().startActivity(intent);
            }
        });
    }

    private void createLoginInfo(LinearLayout user, String name, final View v) {
        LayoutInflater.from(getContext()).inflate(R.layout.item_user_info, user);
        ((TextView) user.findViewById(R.id.settings_user_name)).setText(name);
        user.findViewById(R.id.settings_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.getContext().getSharedPreferences("user", 0).edit()
                        .putString("code", "")
                        .putString("name", "")
                        .apply();
                refreshLoginInfo(v);
            }
        });
    }

    private void refreshLoginInfo(View v) {
        String code = getContext().getSharedPreferences("user", 0).getString("code", "");
        String name = getContext().getSharedPreferences("user", 0).getString("name", "");
        final LinearLayout user = v.findViewById(R.id.settings_user_holder);
        user.removeAllViews();
        if(code.equals("")) {
            createLoginNeeded(user);
        } else {
            createLoginInfo(user, name, v);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshLoginInfo(getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle icicle) {
        View v = inflater.inflate(R.layout.content_settings, container, false);

        String gstring = getActivity().getSharedPreferences("preload", 0).getString("groups", "");
        for(String part : gstring.split(",")) {
            String[] bits = part.split(":");
            if(bits.length < 2) continue;
            groups.put(bits[0], bits[1]);
        }

        LinearLayout layout = v.findViewById(R.id.groups_holder);
        layout.removeAllViews();
        GroupsAdapter adapter = new GroupsAdapter(this, (WorkbooksApplication) getActivity().getApplication(), layout, groups);

        initSwitch(v, R.id.switch_all_exams, "show_only_group_exams", true);
        initSwitch(v, R.id.switch_large_exams, "large_exams", false);
        initSwitch(v, R.id.switch_countdown, "show_countdown", true);
        initSwitch(v, R.id.switch_bottom, "bottom_navigation", false);

        refreshLoginInfo(v);

        return v;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int id) {
        groups.put((String) group.getTag(), (String) getView().findViewById(id).getTag());
        String gstring = "";
        boolean first = true;
        for(Map.Entry<String, String> e : groups.entrySet()) {
            if(!first) gstring += ",";
            first = false;
            gstring += e.getKey() + ":" + e.getValue();
        }
        getActivity().getSharedPreferences("preload", 0).edit().putString("groups", gstring).apply();
        ((WorkbooksApplication) getActivity().getApplication()).fakeRefresh();
    }
}
