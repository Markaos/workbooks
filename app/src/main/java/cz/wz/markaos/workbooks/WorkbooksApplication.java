package cz.wz.markaos.workbooks;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Looper;
import android.util.Log;
import android.os.Handler;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.List;

import cz.wz.markaos.workbooks.util.PreloadManager;
import cz.wz.markaos.workbooks.util.WorkUtil;
import cz.wz.markaos.workbooks.util.notifications.NotificationUtil;

public class WorkbooksApplication extends Application {

    private List<OnRefreshDoneListener> refreshList = new ArrayList<>(3);
    private List<OnRefreshStartedListener> startedList = new ArrayList<>(1);
    private int examId = 0;
    private boolean refreshing = false;
    private GoogleAnalytics analytics;
    private Tracker tracker;

    private class SyncTask extends AsyncTask<WorkbooksApplication, Void, Boolean> {

        public Boolean doInBackground(WorkbooksApplication... params) {
            return PreloadManager.loadAll(params[0]);
        }

        public void onPostExecute(Boolean result) {
            if(result) {
                Log.d("Application", "Refresh done, notifying listeners");
            } else {
                Log.d("Application", "Refresh failed");
            }
            refreshing = false;
            for(OnRefreshDoneListener listener : refreshList) {
                listener.onRefreshDone(result);
            }
        }
    }

    public void onCreate() {
        super.onCreate();
        analytics = GoogleAnalytics.getInstance(this);
        WorkUtil.startAll();
        if(Build.VERSION.SDK_INT >= 26) {
            CharSequence channelName = getString(R.string.channel_exams);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(NotificationUtil.CHANNEL_EXAMS, channelName, importance);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }

    synchronized public Tracker getTracker() {
        if(tracker == null) {
            tracker = analytics.newTracker(R.xml.global_tracker);
        }
        return tracker;
    }

    public interface OnRefreshStartedListener {
        void onRefreshStarted();
    }

    public interface OnRefreshDoneListener {
        void onRefreshDone(boolean success);
    }

    public void addOnRefreshStartedListener(OnRefreshStartedListener listener) {
        startedList.add(listener);
    }

    public void removeOnRefreshStartedListener(OnRefreshStartedListener listener) {
        startedList.remove(listener);
    }

    public void addOnRefreshDoneListener(OnRefreshDoneListener listener) {
        refreshList.add(listener);
    }

    public void removeOnRefreshDoneListener(OnRefreshDoneListener listener) {
        refreshList.remove(listener);
    }

    public void refreshFromBackground() {
        (new Handler(Looper.getMainLooper())).post(new Runnable() {
            @Override
            public void run() {
                refresh();
            }
        });
    }

    public void refresh() {
        if(!refreshing) {
            Log.d("Application", "Refreshing data");
            refreshing = true;
            for(OnRefreshStartedListener listener : startedList) {
                listener.onRefreshStarted();
            }
            SyncTask task = new SyncTask();
            task.execute(this);
        } else {
            Log.w("Application", "Tried to refresh during refresh");
        }
    }

    public void fakeRefresh() {
        for(OnRefreshDoneListener listener : refreshList) {
            listener.onRefreshDone(true);
        }
    }

    public void storeExamId(int id) {
        examId = id;
    }

    public int retrieveExamId() {
        return examId;
    }

    public boolean isRefreshing() {
        return refreshing;
    }
}
