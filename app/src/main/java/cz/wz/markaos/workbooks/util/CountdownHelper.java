package cz.wz.markaos.workbooks.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class CountdownHelper {

    public  static String getText(Context ctx) {
        Calendar date = new GregorianCalendar();
        SharedPreferences prefs = ctx.getSharedPreferences("preload", 0);
        DateFormat format = new SimpleDateFormat("MMM dd, yyyy H:mm:ss", Locale.US);
        int days = -1;
        try {
            long diff = format.parse(prefs.getString("motd_date", "")).getTime() - date.getTimeInMillis();
            double ddiff = (double) diff / (24 * 60 * 60 * 1000);
            days = (int) Math.floor(ddiff);
        } catch (ParseException e) {
            Log.e("CountdownHelper", "Cannot parse date");
            return "Ups...";
        }
        return prefs.getString("motd_start", "").concat(Integer.toString(days).concat(prefs.getString("motd_end", "")));
    }
}
