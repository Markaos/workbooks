package cz.wz.markaos.workbooks.service;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/*import com.google.firebase.appindexing.FirebaseAppIndex;
import com.google.firebase.appindexing.Indexable;
import com.google.firebase.appindexing.builders.Indexables;

import java.util.ArrayList;

import cz.wz.markaos.workbooks.WorkbooksApplication;
import cz.wz.markaos.workbooks.util.database.Contract;

public class AppIndexingService extends IntentService {
    public AppIndexingService() {
        super("AppIndexingService");
        Log.d("AppIndex", "Creating AppIndexingService");
    }

    @Override
    public void onHandleIntent(Intent intent) {
        Log.d("AppIndex", "Updating AppIndex");
        ArrayList<Indexable> workbooks = new ArrayList<>();
        SQLiteDatabase db = new Contract.Helper((WorkbooksApplication) getApplication()).getReadableDatabase();
        String[] columns = {
                Contract.Workbooks._ID,
                Contract.Workbooks.COLUMN_NAME_NAME,
                Contract.Workbooks.COLUMN_NAME_NAME_FULL,
                Contract.Workbooks.COLUMN_NAME_URL
        };
        Cursor c = db.query(Contract.Workbooks.TABLE_NAME, columns, null, null, null, null, null);
        int[] index = {
                c.getColumnIndex(Contract.Workbooks._ID),
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_NAME),
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_NAME_FULL),
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_URL)
        };
        while(c.moveToNext()) {
            Indexable workbookIndex = Indexables.textDigitalDocumentBuilder()
                    .setName(c.getString(index[1]))
                    .setDescription(c.getString(index[2]))
                    .setUrl(c.getString(index[3]))
                    .build();
            workbooks.add(workbookIndex);
        }
        c.close();

        if(workbooks.size() > 0) {
            Indexable[] wbs = new Indexable[workbooks.size()];
            wbs = workbooks.toArray(wbs);

            FirebaseAppIndex.getInstance().update(wbs);
        }
    }
}
*/