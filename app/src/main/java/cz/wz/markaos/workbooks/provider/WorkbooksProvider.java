package cz.wz.markaos.workbooks.provider;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.util.Log;

import cz.wz.markaos.workbooks.WorkbooksApplication;
import cz.wz.markaos.workbooks.util.database.Contract;


public class WorkbooksProvider extends ContentProvider {
    private SQLiteDatabase db = null;

    @Override
    public String getType(@NonNull Uri uri) {
        return "vnd.android.cursor.dir/vnd.cz.markaos.wb.workbooks";
    }

    @Override
    public boolean onCreate() {
        db = new Contract.Helper((WorkbooksApplication) getContext().getApplicationContext()).getReadableDatabase();
        return true;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String orderBy) {
        Log.d("SearchProvider", "query() called");
        String query = uri.getLastPathSegment().toLowerCase();
        String[] proj = {
                BaseColumns._ID,
                SearchManager.SUGGEST_COLUMN_TEXT_1,
                SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID
        };
        MatrixCursor ret = new MatrixCursor(proj);
        String[] columns = {
                Contract.Workbooks._ID,
                Contract.Workbooks.COLUMN_NAME_NAME,
                Contract.Workbooks.COLUMN_NAME_NAME_FULL,
                Contract.Workbooks.COLUMN_NAME_URL
        };
        Cursor c = db.query(Contract.Workbooks.TABLE_NAME, columns, null, null, null, null, null);
        int[] index = {
                c.getColumnIndex(Contract.Workbooks._ID),
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_NAME),
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_NAME_FULL),
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_URL)
        };
        while(c.moveToNext()) {
            String shortName = c.getString(index[1]);
            String fullName = c.getString(index[2]);

            if (
                    (
                            query.length() < 4 &&
                            !shortName.equalsIgnoreCase(query) &&
                            !fullName.equalsIgnoreCase(query)
                    ) || (
                            query.length() >= 4 &&
                            !shortName.toLowerCase().contains(query) &&
                            !fullName.toLowerCase().contains(query)
                    )
            ) {
                continue;
            }

            MatrixCursor.RowBuilder row = ret.newRow();
            row.add(c.getInt(index[0]));                    // BaseColumns._ID
            row.add(c.getString(index[2]));                 // SearchManager.SUGGEST_COLUMN_TEXT_1
            row.add(Integer.toString(c.getInt(index[0])));  // SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID
        }
        c.close();

        return ret;
    }
}
