package cz.wz.markaos.workbooks.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.pixplicity.sharp.Sharp;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.wz.markaos.workbooks.R;
import cz.wz.markaos.workbooks.WorkbooksApplication;
import cz.wz.markaos.workbooks.util.BackgroundGenerator;
import cz.wz.markaos.workbooks.util.IconManager;
import cz.wz.markaos.workbooks.util.database.Contract;

public class WorkbooksAdapter extends RecyclerView.Adapter<WorkbooksAdapter.ViewHolder> {

    private List<Workbook> workbookList;
    private WorkbooksApplication app;

    private class Workbook {
        public File icon;
        public String name, link, archiveUrl;
        public boolean archive;
        public boolean deprecated;

        public Workbook(Context ctx, String icon, String name, String link, String archive, boolean deprecated) {
            this(IconManager.getIconFile(ctx, icon), name, link, archive, deprecated);
        }

        public Workbook(File icon, String name, String link, String archive, boolean deprecated) {
            this.icon = icon;
            this.name = name;
            this.link = link;
            this.archive = archive != null;
            this.archiveUrl = archive;
            this.deprecated = deprecated;
        }
    }

    public WorkbooksAdapter(WorkbooksApplication ctx) {
        app = ctx;
        workbookList = new ArrayList<>();
        SQLiteDatabase db = new Contract.Helper(ctx).getReadableDatabase();
        String[] columns = {
                Contract.Workbooks.COLUMN_NAME_NAME,
                Contract.Workbooks.COLUMN_NAME_URL,
                Contract.Workbooks.COLUMN_NAME_ARCHIVE,
                Contract.Workbooks.COLUMN_NAME_ICON,
                Contract.Workbooks.COLUMN_NAME_DEPRECATED
        };

        Cursor c = db.query(Contract.Workbooks.TABLE_NAME, columns, null, null, null, null, null);
        int[] index = {
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_NAME),
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_URL),
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_ARCHIVE),
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_ICON),
                c.getColumnIndex(Contract.Workbooks.COLUMN_NAME_DEPRECATED)
        };

        while(c.moveToNext()) {
            workbookList.add(new Workbook(
                    ctx,
                    c.getString(index[3]),
                    c.getString(index[0]),
                    c.getString(index[1]),
                    c.getString(index[2]),
                    c.getInt(index[4]) == 1
            ));
        }
        c.close();
        db.close();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView icon;
        public TextView title, archive;
        public View buttonLeft, buttonRight, buttonFull;
        public View item;

        public ViewHolder(View container) {
            super(container);
            icon = (ImageView) container.findViewById(R.id.item_workbook_icon);
            title = (TextView) container.findViewById(R.id.item_workbook_name);
            archive = (TextView) container.findViewById(R.id.item_workbook_archive);
            buttonLeft = container.findViewById(R.id.item_workbook_overlay_left);
            buttonRight = container.findViewById(R.id.item_workbook_overlay_right);
            buttonFull = container.findViewById(R.id.item_workbook_overlay_whole);
            item = container;

            /*if(Build.VERSION.SDK_INT >= 21) {
                buttonFull.setBackgroundTintList(container.getResources().getColorStateList(R.color.selector_button));
                buttonLeft.setBackgroundTintList(container.getResources().getColorStateList(R.color.selector_button));
                buttonRight.setBackgroundTintList(container.getResources().getColorStateList(R.color.selector_button));
            } else if(Build.VERSION.SDK_INT >= 16) {

            }*/
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_workbook_button, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, final int position) {
        boolean dep = workbookList.get(position).deprecated;
        if(Build.VERSION.SDK_INT >= 21) {
            vh.item.getBackground().setTint(BackgroundGenerator.getWorkbookColor(dep));
        } else if (Build.VERSION.SDK_INT >= 16) {
            vh.item.setBackground(BackgroundGenerator.generateWorkbook(vh.item.getContext(), R.drawable.button_workbook, dep));
        }
        if(workbookList.get(position).icon.exists()) {
            Sharp.loadFile(workbookList.get(position).icon).into(vh.icon);
        }
        final String title = workbookList.get(position).name;
        vh.title.setText(title);
        vh.archive.setVisibility(workbookList.get(position).archive ? View.VISIBLE : View.INVISIBLE);
        if(workbookList.get(position).archive) {
            vh.buttonFull.setVisibility(View.GONE);
            vh.buttonLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(workbookList.get(position).link));
                    app.getTracker().send(new HitBuilders.EventBuilder()
                        .setCategory("Open WB")
                        .setAction(title)
                        .build());
                    v.getContext().startActivity(intent);
                }
            });
            vh.buttonRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(workbookList.get(position).archiveUrl));
                    app.getTracker().send(new HitBuilders.EventBuilder()
                            .setCategory("Open Archive")
                            .setAction(title)
                            .build());
                    v.getContext().startActivity(intent);
                }
            });
        } else {
            vh.buttonFull.setVisibility(View.VISIBLE);
            vh.buttonFull.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(workbookList.get(position).link));
                    app.getTracker().send(new HitBuilders.EventBuilder()
                            .setCategory("Open WB")
                            .setAction(title)
                            .build());
                    v.getContext().startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return workbookList.size();
    }
}
