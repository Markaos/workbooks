package cz.wz.markaos.workbooks;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cz.wz.markaos.workbooks.util.PreloadManager;

public class PreloaderActivity extends AppCompatActivity {

    private class PreloaderTask extends AsyncTask<Void, Void, Boolean> {

        public Boolean doInBackground(Void... params) {
            return PreloadManager.loadAll((WorkbooksApplication) getApplication());
        }

        public void onPostExecute(Boolean result) {
            if(result) {
                ((TextView) findViewById(R.id.preloader_title)).setText(R.string.preload_done);
                findViewById(R.id.progressBar2).setVisibility(View.INVISIBLE);
                findViewById(R.id.preloader_done).setVisibility(View.VISIBLE);
                findViewById(R.id.button).setVisibility(View.VISIBLE);
                getSharedPreferences("preload", 0).edit().putBoolean("loaded", true).apply();
            } else {
                ((TextView) findViewById(R.id.preloader_title)).setText(R.string.preload_failure);
                findViewById(R.id.progressBar2).setVisibility(View.INVISIBLE);
                findViewById(R.id.preloader_failure).setVisibility(View.VISIBLE);
                findViewById(R.id.button_again).setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preloader);
        ((Button) findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), GroupSelectionActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
            }
        });

        ((Button) findViewById(R.id.button_again)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.progressBar2).setVisibility(View.VISIBLE);
                findViewById(R.id.preloader_failure).setVisibility(View.INVISIBLE);
                findViewById(R.id.button_again).setVisibility(View.INVISIBLE);
                new PreloaderTask().execute();
            }
        });

        if(getSharedPreferences("preload", 0).getBoolean("groupsUpdate", false)) {
            ((TextView) findViewById(R.id.preloader_notice)).setText(R.string.preload_notice_groups_update);
        }
        new PreloaderTask().execute();
    }
}
