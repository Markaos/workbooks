package cz.wz.markaos.workbooks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cz.wz.markaos.workbooks.adapters.RecyclerExamsAdapter;
import cz.wz.markaos.workbooks.layout.AutoGridLayoutManager;

import static android.view.View.GONE;

public class ExamsFragment extends Fragment implements WorkbooksApplication.OnRefreshDoneListener, PopupMenu.OnMenuItemClickListener {

    public interface OnRemoteClickListener {
        void onRemoteClick(View v);
    }

    private List<OnRemoteClickListener> listeners;

    public void click(@Nullable View v) {
        for(OnRemoteClickListener listener : listeners) {
            listener.onRemoteClick(v);
        }

        View content = getView();
        if(content != null) content.findViewById(R.id.exams_overlay).setVisibility(GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle icicle) {
        View v = inflater.inflate(R.layout.content_exams, container, false);

        ((WorkbooksApplication) getActivity().getApplication()).addOnRefreshDoneListener(this);

        int examSize = 150;
        if(getContext().getSharedPreferences("prefs", 0).getBoolean("large_exams", false)) {
            examSize = 250;
        }

        RecyclerView view = (RecyclerView) v.findViewById(R.id.exams_recycler);
        final RecyclerExamsAdapter adapter = new RecyclerExamsAdapter((WorkbooksApplication) getActivity().getApplication(), this);
        final GridLayoutManager manager = new AutoGridLayoutManager(
                getContext(),
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, examSize, getContext().getResources().getDisplayMetrics()),
                LinearLayoutManager.VERTICAL,
                false
        );
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return adapter.getItemViewType(position) == RecyclerExamsAdapter.BUTTON ? manager.getSpanCount() : 1;
            }
        });
        view.setAdapter(adapter);
        view.setLayoutManager(manager);

        v.findViewById(R.id.exams_overlay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click(view);
            }
        });

        return v;
    }

    @Override
    public void onRefreshDone(boolean success) {
        if(!success) return;
        Log.d("Exams", "Handling refresh");
        View v = getView();
        if(v != null) {
            int examSize = 150;
            if(getContext().getSharedPreferences("prefs", 0).getBoolean("large_exams", false)) {
                examSize = 250;
            }

            RecyclerView view = (RecyclerView) v.findViewById(R.id.exams_recycler);
            final RecyclerExamsAdapter adapter = new RecyclerExamsAdapter((WorkbooksApplication) getActivity().getApplication(), this);
            final GridLayoutManager manager = new AutoGridLayoutManager(
                    getContext(),
                    (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, examSize, getContext().getResources().getDisplayMetrics()),
                    LinearLayoutManager.VERTICAL,
                    false
            );
            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return adapter.getItemViewType(position) == RecyclerExamsAdapter.BUTTON ? manager.getSpanCount() : 1;
                }
            });
            view.setAdapter(adapter);
            view.setLayoutManager(manager);

            v.findViewById(R.id.exams_overlay).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    click(view);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onRefreshDone(true);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_em_delete) {
            Intent intent = new Intent(getContext(), DeleteExamActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_em_update) {
            Intent intent = new Intent(getContext(), ModifyExamActivity.class);
            startActivity(intent);
            return true;
        }

        return false;
    }
}
