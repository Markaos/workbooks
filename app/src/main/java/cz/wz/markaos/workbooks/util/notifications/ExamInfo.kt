package cz.wz.markaos.workbooks.util.notifications

data class ExamInfo (val id: Int, val subject: String, val range: String, val date: String, val dateLong: Long)