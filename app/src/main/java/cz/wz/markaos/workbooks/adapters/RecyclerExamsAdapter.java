package cz.wz.markaos.workbooks.adapters;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import cz.wz.markaos.workbooks.AddExamActivity;
import cz.wz.markaos.workbooks.R;
import cz.wz.markaos.workbooks.WorkbooksApplication;
import cz.wz.markaos.workbooks.util.database.Contract;

public class RecyclerExamsAdapter extends RecyclerView.Adapter<RecyclerExamsAdapter.ViewHolder> {

    private class Exam {
        public String subject, title, date, link;
        public int daysRemaining, id;

        public Exam(String subject, String title, String date, String link, int daysRemaining, int id) {
            this.subject = subject;
            this.title = title;
            this.date = date;
            this.daysRemaining = daysRemaining;
            this.id = id;
            this.link = link;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView subject, title, date;
        public View container, attachment;

        public ViewHolder(View v, int type) {
            super(v);
            if(type == EXAM) {
                subject = (TextView) v.findViewById(R.id.item_exam_subject);
                title = (TextView) v.findViewById(R.id.item_exam_title);
                date = (TextView) v.findViewById(R.id.item_exam_date);
                attachment = v.findViewById(R.id.icon_link);
            }
            container = v;
        }
    }

    public static final int EXAM = 1;
    public static final int BUTTON = 2;

    private List<Exam> examList;
    private PopupMenu.OnMenuItemClickListener listener;
    private WorkbooksApplication ctx;

    public RecyclerExamsAdapter(WorkbooksApplication ctx, PopupMenu.OnMenuItemClickListener listener) {
        this.listener = listener;
        this.ctx = ctx;
        examList = new ArrayList<>();
        SQLiteDatabase db = new Contract.Helper(ctx).getReadableDatabase();
        String[] columns = {
                Contract.Exams.COLUMN_NAME_SUBJECT,
                Contract.Exams.COLUMN_NAME_RANGE,
                Contract.Exams.COLUMN_NAME_DATE,
                Contract.Exams.COLUMN_NAME_GROUP,
                Contract.Exams.COLUMN_NAME_ID,
                Contract.Exams.COLUMN_NAME_LINK
        };
        Cursor c = db.query(Contract.Exams.TABLE_NAME, columns, null, null, null, null, Contract.Exams.COLUMN_NAME_DATE_I + " ASC");
        int[] index = {
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_SUBJECT),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_RANGE),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_DATE),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_GROUP),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_ID),
                c.getColumnIndex(Contract.Exams.COLUMN_NAME_LINK)
        };

        Calendar date = new GregorianCalendar();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        String groups = ctx.getSharedPreferences("preload", 0).getString("groups", "NONE");
        boolean showAll = !ctx.getSharedPreferences("prefs", 0).getBoolean("show_only_group_exams", true);

        while(c.moveToNext()) {
            String group = c.getString(index[3]);
            if(!showAll && !group.equals("none") && !groups.contains(group)) {
                continue;
            }
            SimpleDateFormat f = new SimpleDateFormat("d.M.y", Locale.US);
            int days = -1;
            try {
                long diff = f.parse(c.getString(index[2])).getTime() - date.getTimeInMillis();
                double ddiff = (double) diff / (24 * 60 * 60 * 1000);
                if(ddiff > 0) days = (int) Math.ceil(ddiff);
                else days = (int) Math.floor(ddiff);
            } catch (ParseException e) {
                Log.e("Exams", "Cannot parse date " + c.getString(index[2]));
            }
            examList.add(new RecyclerExamsAdapter.Exam(
                    c.getString(index[0]),
                    c.getString(index[1]),
                    c.getString(index[2]),
                    c.getString(index[5]),
                    days,
                    c.getInt(index[4])
            ));
        }
        c.close();
        db.close();
    }

    public int getItemCount() {
        return examList.size() + 1;
    }

    public int getItemViewType(int position) {
        return position == examList.size() ? BUTTON : EXAM;
    }

    public ViewHolder onCreateViewHolder(ViewGroup root, int type) {
        View v = LayoutInflater.from(root.getContext())
                .inflate(type == EXAM ? R.layout.item_exam : R.layout.item_button_add, root, false);
        if(type == BUTTON) {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), AddExamActivity.class);
                    view.getContext().startActivity(intent);
                }
            });
        } else {
            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    PopupMenu menu = new PopupMenu(view.getContext(), view);
                    menu.inflate(R.menu.menu_exam);
                    menu.setOnMenuItemClickListener(listener);
                    ctx.storeExamId((int) view.getTag());
                    menu.show();
                    return true;
                }
            });
        }
        return new ViewHolder(v, type);
    }

    public void onBindViewHolder(ViewHolder vh, int position) {
        int type = getItemViewType(position);
        if(type == EXAM) {
            final Exam exam = examList.get(position);
            vh.subject.setText(exam.subject);
            vh.title.setText(exam.title);
            vh.date.setText(exam.date);
            vh.container.setTag(exam.id);
            if(exam.link.equals("none")) {
                vh.container.setOnClickListener(null);
                vh.attachment.setVisibility(View.GONE);
            } else {
                vh.container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(exam.link));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ctx.startActivity(intent);
                        } catch (RuntimeException e) {
                            Log.e("RecyclerExamsAdapter", "Couldn't start activity", e);
                        }
                    }
                });
                vh.attachment.setVisibility(View.VISIBLE);
            }
        }
    }
}
