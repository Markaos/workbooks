package cz.wz.markaos.workbooks;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import cz.wz.markaos.workbooks.adapters.WorkbooksAdapter;
import cz.wz.markaos.workbooks.util.LinearLayoutWrapper;

public class ListFragment extends Fragment implements WorkbooksApplication.OnRefreshDoneListener {

    boolean resetList = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle icicle) {
        View v = inflater.inflate(R.layout.content_main, container, false);

        ((WorkbooksApplication) getActivity().getApplication()).addOnRefreshDoneListener(this);

        LinearLayout layout = (LinearLayout) v.findViewById(R.id.list_container);
        LinearLayoutWrapper wrapper = new LinearLayoutWrapper(layout);
        wrapper.setAdapter(new WorkbooksAdapter((WorkbooksApplication) getActivity().getApplication()));

        return v;
    }

    @Override
    public void onRefreshDone(boolean success) {
        if(success) {
            Log.d("List", "Handling refresh");
            resetList = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(resetList) {
            Log.d("List", "Updating list");
            resetList = false;

            View v = getView();
            if(v == null) return;

            LinearLayout layout = (LinearLayout) v.findViewById(R.id.list_container);
            layout.removeAllViews();
            LinearLayoutWrapper wrapper = new LinearLayoutWrapper(layout);
            wrapper.setAdapter(new WorkbooksAdapter((WorkbooksApplication) getActivity().getApplication()));
        }
    }
}
