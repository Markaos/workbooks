package cz.wz.markaos.workbooks;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, ListActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        //getSharedPreferences("preload", 0).edit().putBoolean("groupsUpdate", false).apply();
        finish();
    }
}
