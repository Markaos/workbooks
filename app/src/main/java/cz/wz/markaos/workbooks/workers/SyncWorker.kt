package cz.wz.markaos.workbooks.workers

import android.util.Log
import androidx.work.Worker
import cz.wz.markaos.workbooks.util.PreloadManager

class SyncWorker : Worker() {

    override fun doWork(): WorkerResult {
        Log.d("SyncWorker", "SyncWorker called")
        if (PreloadManager.loadAll(applicationContext)) {
            return WorkerResult.SUCCESS
        } else {
            return WorkerResult.RETRY
        }
    }
}