package cz.wz.markaos.workbooks;

import android.app.ActivityManager;
import android.support.v4.app.Fragment;
import cz.wz.markaos.workbooks.ListFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import cz.wz.markaos.workbooks.adapters.OurPagerAdapter;
import cz.wz.markaos.workbooks.util.CountdownHelper;

public class ListActivity extends AppCompatActivity implements
        WorkbooksApplication.OnRefreshStartedListener,
        WorkbooksApplication.OnRefreshDoneListener,
        BottomNavigationView.OnNavigationItemSelectedListener {

    private int mLayout;
    private ViewPager mPager;
    private BottomNavigationView mBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences("preload", 0);
        if(!preferences.getBoolean("loaded", false) || !preferences.getBoolean("groupsUpdate", false)) {
            startActivity(new Intent(this, PreloaderActivity.class));
            finish();
            return;
        }
        mLayout = getSharedPreferences("prefs", 0).getBoolean("bottom_navigation", false) ? R.layout.activity_main_bottom_real : R.layout.activity_main;
        setContentView(mLayout);

        prepareContent();

        final WorkbooksApplication application = (WorkbooksApplication) getApplication();

        application.addOnRefreshDoneListener(this);
        application.addOnRefreshStartedListener(this);
        if(application.isRefreshing()) {
            onRefreshStarted();
        }

        ((TextView) findViewById(R.id.countdown)).setText(CountdownHelper.getText(this));
        boolean countdown = getSharedPreferences("prefs", 0).getBoolean("show_countdown", true);
        findViewById(R.id.countdown).setVisibility(countdown ? View.VISIBLE : View.GONE);
    }

    private void prepareContent() {
        final Tracker tracker = ((WorkbooksApplication) getApplication()).getTracker();

        tracker.setScreenName("Workbooks");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());

        final WorkbooksApplication application = (WorkbooksApplication) getApplication();

        mPager = (ViewPager) findViewById(R.id.main_pager);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Not important
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        tracker.setScreenName("Workbooks");
                        if (mBottomNavigationView != null)
                            mBottomNavigationView.setSelectedItemId(R.id.navigation_workbooks);
                        break;
                    case 1:
                        tracker.setScreenName("Exams");
                        if (mBottomNavigationView != null)
                            mBottomNavigationView.setSelectedItemId(R.id.navigation_exams);
                        break;
                    case 2:
                        tracker.setScreenName("Settings");
                        if (mBottomNavigationView != null)
                            mBottomNavigationView.setSelectedItemId(R.id.navigation_settings);
                        break;
                }
                Log.i("Bullshit", "Sending hit for screen " + position);
                tracker.send(new HitBuilders.ScreenViewBuilder().build());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Not important
            }
        });
        mPager.setAdapter(new OurPagerAdapter(getSupportFragmentManager(), this));

        if (mLayout == R.layout.activity_main || mLayout == R.layout.activity_main_bottom) {
            ((TabLayout) findViewById(R.id.tabs)).setupWithViewPager(mPager);

            findViewById(R.id.button_refresh).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    application.refresh();
                }
            });
            setTheme(R.style.AppTheme_NoActionBar_Light_Content);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window w = getWindow();
                w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                setTheme(R.style.AppTheme_NoActionBar_Light_NoStatusBar);
            }
            mBottomNavigationView = findViewById(R.id.tabs);
            mBottomNavigationView.setOnNavigationItemSelectedListener(this);
            mBottomNavigationView.setSelectedItemId(R.id.navigation_workbooks);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id;
        switch (item.getItemId()) {
            case R.id.navigation_refresh:
                ((WorkbooksApplication) getApplication()).refresh();
                return false;
            default:
            case R.id.navigation_workbooks: id = 0; break;
            case R.id.navigation_exams: id = 1; break;
            case R.id.navigation_settings: id = 2; break;
        }
        mPager.setCurrentItem(id);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.countdown)).setText(CountdownHelper.getText(this));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            ActivityManager.TaskDescription td = new ActivityManager.TaskDescription(null, bm, getColor(R.color.colorPrimaryDarkBright));

            setTaskDescription(td);
            bm.recycle();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            ((WorkbooksApplication) getApplication()).refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefreshStarted() {
        if (mLayout == R.layout.activity_main || mLayout == R.layout.activity_main_bottom) {
            findViewById(R.id.button_refresh).setVisibility(View.GONE);
            findViewById(R.id.progress_refresh).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefreshDone(boolean success) {
        if (mLayout == R.layout.activity_main || mLayout == R.layout.activity_main_bottom) {
            findViewById(R.id.button_refresh).setVisibility(View.VISIBLE);
            findViewById(R.id.progress_refresh).setVisibility(View.GONE);
        }
        ((TextView) findViewById(R.id.countdown)).setText(CountdownHelper.getText(this));
        boolean countdown = getSharedPreferences("prefs", 0).getBoolean("show_countdown", true);
        findViewById(R.id.countdown).setVisibility(countdown ? View.VISIBLE : View.GONE);
    }
}
